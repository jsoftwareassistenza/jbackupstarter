/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbackupstarter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.crypto.Data;

/**
 *
 * @author pgx71
 */
public class Main {

    public static Process process = null;
//    public static Process processGui = null;
    public static EseguiJBackup ese = null;
//    public static EseguiJBackupGui eseGui = null;
    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        // inizializzazione
        Runtime.getRuntime().addShutdownHook(new JBackupShutdown());
//        FileOutputStream f = null;
//        try {
//            f = new FileOutputStream("backuplog.txt");
//            f.write("start\r\n".getBytes());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        File fp = new File("env" + File.separator + "logger_starter");

        if (!fp.exists()) {
            try {
                fp.mkdir();
            } catch (Exception ex) {
                ex.printStackTrace();

            }
        }

        FileHandler fh;
        try {
            fh = new FileHandler("env" + File.separator + "logger_starter" + File.separator + "logger_starter.log", 10000, 5, true);
            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

        GestoreCFG g = new GestoreCFG("jbackupstarter.ini");
        if (!g.esisteFile()) {
            g.creaFile(new Hashtable());
            g.aggiornaChiave("IND_DOWNLOAD_FILE", "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackup_agg.zip");
            g.aggiornaChiave("IND_DOWNLOAD_LIB", "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackup_agg_lib.zip");
            g.aggiornaChiave("IND_DOWNLOAD_INFOAGG", "http://www.jazzgestionale.it/sito/download/JBACKUP2/info_jbackup_agg.txt");
            g.aggiornaChiave("PROXY", "N");
        }

        File faggx = new File("agg");
        if (!faggx.exists()) {
            faggx.mkdir();
        }

        // lancia l'applicazione
        System.out.println("JBACKUP STARTER: lancio JBackup New");
        logger.log(Level.INFO, "JBACKUP STARTER NEW");
        ese = new EseguiJBackup();
        ese.start();
//        if (g.leggiChiave("SERVICE").equals("S")) {
//            ese = new EseguiJBackup();
//            ese.start();
//            try {
//
//                f.write("start service\r\n".getBytes());
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }

        int num = 0;
        while (true) {

//            FileOutputStream f = null;
            try {
//            f = new FileOutputStream("backupstarter" + num + ".txt");
//            f.write("start\r\n".getBytes());

//            System.out.println("Process: " + processGui);
//
//            boolean run = false;
//            if (processGui != null) {
//                try {
//                    processGui.exitValue();
//                    run = false;
//
//                } catch (Exception e) {
//                    run = true;
//                }
//            }
//            System.out.println("Process: " + run);
//            if (g.leggiChiave("SERVICE").equals("S") && g.leggiChiave("GUI").equals("S") && (processGui == null || !run)) {
//                try {
//
//                    f.write("start gui\r\n".getBytes());
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                eseGui = new EseguiJBackupGui();
//                eseGui.start();
//                g.aggiornaChiave("GUI", "N");
//                System.out.println("" + processGui);
//            } else if (!g.leggiChiave("SERVICE").equals("S") && !run && g.leggiChiave("GUI").equals("S")) {
//                try {
//
//                    f.write("start gui2\r\n".getBytes());
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                eseGui = new EseguiJBackupGui();
//                eseGui.start();
//                g.aggiornaChiave("GUI", "N");
//
//            }
//            try {
//                f.write("uscito\r\n".getBytes());
//                f.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
                File fbackupincorso = new File("backupincorso.txt");
                boolean bic = (fbackupincorso.exists());
                if (!bic) {
//                f.write("file backupincorso non esiste\r\n".getBytes());
                    // controllo aggiornamenti
                    boolean agg = false;
                    GestoreCFG gc = new GestoreCFG("jbackupstarter.ini");
                    // dati link download
                    String protfile = gc.leggiChiave("IND_DOWNLOAD_FILE");
                    if (protfile.equals("")) {
                        protfile = "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackup_agg.zip";
                    }
                    String protlib = gc.leggiChiave("IND_DOWNLOAD_LIB");
                    if (protlib.equals("")) {
                        protlib = "http://www.jazzgestionale.it/sito/download/JBACKUP2/jbackup_agg_lib.zip";
                    }
                    String protinfoagg = gc.leggiChiave("IND_DOWNLOAD_INFOAGG");
                    if (protinfoagg.equals("")) {
                        protinfoagg = "http://www.jazzgestionale.it/sito/download/JBACKUP2/info_jbackup_agg.txt";
                    }
                    // dati proxy
                    boolean usaproxy = (gc.leggiChiave("USAPROXY").equals("S"));
                    String indproxy = gc.leggiChiave("INDPROXY");
                    String portaproxy = gc.leggiChiave("PORTAPROXY");
                    String userproxy = gc.leggiChiave("USERPROXY");
                    String pwdproxy = gc.leggiChiave("PWDPROXY");
                    boolean autproxy = (!userproxy.equals(""));
                    Proxy proxy = null;
                    if (usaproxy) {
                        proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(indproxy, estraiIntero(portaproxy)));
                    }
                    long lf = 0;
                    long lflib = 0;
                    long dimfinfoagg = 0;
                    long dimlibinfoagg = 0;
                    FileOutputStream fos = null;
                    try {
                        boolean errdownloadinfo = false;
                        boolean errdownloadprog = false;
                        boolean errdownloadlib = false;
                        boolean download = false;

                        // controllo file info
                        boolean contenutoinfopres = false;
                        try {
                            URL urlAggInfo = new URL(protinfoagg);
                            if (usaproxy) {
                                URLConnection uc = urlAggInfo.openConnection(proxy);
                                if (autproxy) {
                                    String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                    uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                }
                                contenutoinfopres = (uc.getContent() != null);
                            } else {
                                contenutoinfopres = (urlAggInfo.openConnection().getContent() != null);
                            }
                            if (contenutoinfopres) {
                                System.out.println("JBACKUP STARTER: Download info_jbackup_agg.txt");
//                            f.write("JBACKUP STARTER: Download info_jbackup_agg.txt\r\n".getBytes());
                                try {
                                    fos = new FileOutputStream("agg" + File.separator + "info_jbackup_agg.txt");
                                    int diminfo = 0;
                                    InputStream in = null;
                                    if (usaproxy) {
                                        URLConnection uc = urlAggInfo.openConnection(proxy);
                                        if (autproxy) {
                                            String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                            uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                        }
                                        in = uc.getInputStream();
                                        diminfo = uc.getContentLength();
                                    } else {
                                        in = urlAggInfo.openStream();
                                        //dimf = urlAggProg.openConnection().getContentLength();
                                        URLConnection uc = urlAggInfo.openConnection();
                                        //uc.setConnectTimeout(3000);
                                        diminfo = uc.getContentLength();
                                    }
                                    for (int i = 0; i < diminfo; i++) {
                                        byte[] b = new byte[1];
                                        in.read(b);
                                        fos.write(b);
                                    }
                                     
                                    fos.close();
                                   
                                    in.close();
                                } catch (Exception ejdaa) {
                                    errdownloadinfo = true;
//                                f.write("JBACKUP STARTER: errore su download info_jbackup_agg.txt\r\n".getBytes());
                                    System.out.println("JBACKUP STARTER: errore su download info_jbackup_agg.txt");
                                    logger.log(Level.INFO, "JBACKUP STARTER: errore su download info_jbackup_agg.txt");
                                    try {
                                        fos.close();
                                    } catch (Exception efos) {
                                    }
                                }
                                if (!errdownloadinfo) {
                                    GestoreCFG gi = new GestoreCFG("agg" + File.separator + "info_jbackup_agg.txt");
                                    if (!gi.leggiChiave("DIMPROG").equals("")) {
                                        dimfinfoagg = estraiLong(gi.leggiChiave("DIMPROG"));
                                    }
                                    if (!gi.leggiChiave("DIMLIB").equals("")) {
                                        dimlibinfoagg = estraiLong(gi.leggiChiave("DIMLIB"));
                                    }
                                }
                            }
                        } catch (Exception einfo) {

                        }
                        if (!contenutoinfopres) {
                            logger.log(Level.INFO, "JBACKUP STARTER: info_jbackup_agg.txt non presente su sito");
                            System.out.println("JBACKUP STARTER: info_jbackup_agg.txt non presente su sito");
//                        f.write("non presente\r\n".getBytes());
                        } else {
                            long uaggprog = estraiLong(gc.leggiChiave("ultimo_agg_prog"));
                            logger.log(Level.INFO, "JBACKUP STARTER: aggiornamento attuale programma: {0}", uaggprog);
                            System.out.println("JBACKUP STARTER: aggiornamento attuale programma: " + uaggprog);
//                        f.write("1\r\n".getBytes());
                            long uagglib = estraiLong(gc.leggiChiave("ultimo_agg_lib"));
                            logger.log(Level.INFO, "JBACKUP STARTER: aggiornamento attuale lib: {0}", uagglib);
                            System.out.println("JBACKUP STARTER: aggiornamento attuale lib: " + uagglib);
//                        f.write("2\r\n".getBytes());
                            URL urlAggProg = new URL(protfile);
                            if (usaproxy) {
                                URLConnection uc = urlAggProg.openConnection(proxy);
                                if (autproxy) {
                                    String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                    uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                }
                                lf = uc.getLastModified();
                            } else {
                                lf = urlAggProg.openConnection().getLastModified();
                            }
                            System.out.println("JBACKUP STARTER: aggiornamento programma sul sito: " + lf);
//                        f.write("3\r\n".getBytes());
                            if (lf > 55) {
                                // scarica aggiornamento programma
                                boolean contenutopres = false;
                                if (usaproxy) {
                                    URLConnection uc = urlAggProg.openConnection(proxy);
                                    if (autproxy) {
                                        String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                        uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                    }
                                    contenutopres = (uc.getContent() != null);
                                } else {
                                    contenutopres = (urlAggProg.openConnection().getContent() != null);
                                }
                                if (contenutopres) {
                                    download = true;
                                    System.out.println("JBACKUP STARTER: Download aggiornamento jbackup_agg.zip");
                                    logger.log(Level.INFO, "JBACKUP STARTER: Download aggiornamento jbackup_agg.zip");
//                                f.write("4\r\n".getBytes());
                                    int dimf = 0;
                                    try {
                                        fos = new FileOutputStream("agg" + File.separator + "jbackup_agg.zip");
                                        InputStream in = null;
                                        if (usaproxy) {
                                            URLConnection uc = urlAggProg.openConnection(proxy);
                                            if (autproxy) {
                                                String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                                uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                            }
                                            in = uc.getInputStream();
                                            dimf = uc.getContentLength();
                                        } else {
                                            in = urlAggProg.openStream();
                                            //dimf = urlAggProg.openConnection().getContentLength();
                                            URLConnection uc = urlAggProg.openConnection();
                                            //uc.setConnectTimeout(3000);
                                            dimf = uc.getContentLength();
                                        }
                                        for (int i = 0; i < dimf; i++) {
                                            byte[] b = new byte[1];
                                            in.read(b);
                                            fos.write(b);
                                        }
                                        fos.close();
                                        in.close();
                                    } catch (Exception ejdaa) {
                                        errdownloadprog = true;
                                        logger.log(Level.INFO, "JBACKUP STARTER: errore su download jbackup_agg.zip");
                                        System.out.println("JBACKUP STARTER: errore su download jbackup_agg.zip");
//                                    f.write("5\r\n".getBytes());
                                        try {
                                            fos.close();
                                        } catch (Exception efos) {
                                        }
                                    }
                                    if (!errdownloadprog) {
                                        logger.log(Level.INFO, "JBACKUP STARTER: Download aggiornamento jbackup_agg.zip OK");
                                        System.out.println("JBACKUP STARTER: Download aggiornamento jbackup_agg.zip OK");
//                                    f.write("6\r\n".getBytes());
                                    }
                                }
                            }
                            if (!errdownloadprog) {
                                URL urlAggLib = new URL(protlib);
                                if (usaproxy) {
                                    URLConnection uc = urlAggLib.openConnection(proxy);
                                    if (autproxy) {
                                        String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                        uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                    }
                                    lflib = uc.getLastModified();
                                } else {
                                    lflib = urlAggLib.openConnection().getLastModified();
                                }
                                logger.log(Level.INFO, "JBACKUP STARTER: aggiornamento lib sul sito: {0}" , lflib);
                                System.out.println("JBACKUP STARTER: aggiornamento lib sul sito: " + lflib);
//                            f.write("7\r\n".getBytes());
                                if (lflib > uagglib) {
                                    // scarica aggiornamento lib
                                    boolean contenutopres = false;
                                    if (usaproxy) {
                                        URLConnection uc = urlAggLib.openConnection(proxy);
                                        if (autproxy) {
                                            String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                            uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                        }
                                        contenutopres = (uc.getContent() != null);
                                    } else {
                                        contenutopres = (urlAggLib.openConnection().getContent() != null);
                                    }
                                    if (contenutopres) {
                                        download = true;
                                        System.out.println("JBACKUP STARTER: Download aggiornamento jbackup_agg_lib.zip...");
                                        logger.log(Level.INFO, "JBACKUP STARTER: Download aggiornamento jbackup_agg_lib.zip...");
//                                    f.write("8\r\n".getBytes());
                                        int dimf = 0;
                                        try {
                                            fos = new FileOutputStream("agg" + File.separator + "jbackup_agg_lib.zip");
                                            InputStream in = null;
                                            if (usaproxy) {
                                                URLConnection uc = urlAggLib.openConnection(proxy);
                                                if (autproxy) {
                                                    String encoded = new String(Base64Coder.encodeString(new String(userproxy + ":" + pwdproxy)));
                                                    uc.setRequestProperty("Proxy-Authorization", "Basic " + encoded);
                                                }
                                                in = uc.getInputStream();
                                                dimf = uc.getContentLength();
                                            } else {
                                                in = urlAggLib.openStream();
                                                //dimf = urlAggProg.openConnection().getContentLength();
                                                URLConnection uc = urlAggLib.openConnection();
                                                //uc.setConnectTimeout(3000);
                                                dimf = uc.getContentLength();
                                            }
                                            for (int i = 0; i < dimf; i++) {
                                                byte[] b = new byte[1];
                                                in.read(b);
                                                fos.write(b);
                                            }
                                            fos.close();
                                            in.close();
                                        } catch (Exception ejdaa) {
                                            errdownloadlib = true;
                                            System.out.println("JBACKUP STARTER: errore su download jbackup_agg_lib.zip");
                                            logger.log(Level.INFO, "JBACKUP STARTER: errore su download jbackup_agg_lib.zip");
//                                        f.write("9\r\n".getBytes());
                                            try {
                                                fos.close();
                                            } catch (Exception efos) {
                                            }
                                        }
                                        if (!errdownloadlib) {
                                            System.out.println("JBACKUP STARTER: Download aggiornamento jbackup_agg_lib.zip OK");
                                            logger.log(Level.INFO, "JBACKUP STARTER: Download aggiornamento jbackup_agg_lib.zip OK");
//                                        f.write("10\r\n".getBytes());
                                        }
                                    }
                                }
                            }
                            if (download) {
                                if (errdownloadprog || errdownloadlib) {
                                    svuotaDirAgg();
                                    agg = false;
                                } else {
                                    // verifica dimensioni aggiornamenti scaricati
                                    boolean integritaagg = true;
                                    File fx = new File("agg" + File.separator + "jbackup_agg.zip");
                                    if (fx.exists() && dimfinfoagg > 0 && fx.length() != dimfinfoagg) {
                                        integritaagg = false;
                                        System.out.println("JBACKUP STARTER: Problema integrità aggiornamento scaricato");
                                        System.out.println("JBACKUP STARTER: jbackup_agg.zip dim.locale:" + fx.length() + " dim.info.agg:" + dimfinfoagg);
                                        logger.log(Level.INFO, "JBACKUP STARTER: Problema integrità aggiornamento scaricato");

//                                    f.write("11\r\n".getBytes());
                                    }
                                    File fx2 = new File("agg" + File.separator + "jbackup_agg_lib.zip");
                                    if (fx2.exists() && dimlibinfoagg > 0 && fx2.length() != dimlibinfoagg) {
                                        integritaagg = false;
                                        System.out.println("JBACKUP STARTER: Problema integrità aggiornamento scaricato");
                                        System.out.println("JBACKUP STARTER: jbackup_agg_lib.zip dim.locale:" + fx2.length() + " dim.info.agg:" + dimlibinfoagg);
                                        logger.log(Level.INFO, "JBACKUP STARTER: Problema integrità aggiornamento scaricato");
//                                    f.write("12\r\n".getBytes());
                                    }
                                    if (!integritaagg) {
                                        svuotaDirAgg();
                                        agg = false;
                                    } else {
                                        agg = true;
                                    }
                                }
                            }
                        }
                    } catch (Exception eagg) {
                        eagg.printStackTrace();
                        agg = false;
                    }

                    if (agg) {
                        // chiusura applicazione
                        boolean errchiusura = false;
                        System.out.println("JBACKUP STARTER: chiusura applicazione in corso...");
                        logger.log(Level.INFO, "JBACKUP STARTER: chiusura applicazione in corso...");
//                    f.write("13\r\n".getBytes());
                        try {
//                        if (processGui != null) {
//                            processGui.destroy();
//                        }
                            if (process != null) {
                                process.destroy();
                            }
                        } catch (Exception ex) {
                            errchiusura = true;
                            ex.printStackTrace();
                        }
                        if (!errchiusura) {
                            System.out.println("JBACKUP STARTER: chiusura applicazione OK");
                            logger.log(Level.INFO, "JBACKUP STARTER: chiusura applicazione OK");
//                        f.write("14\r\n".getBytes());
                            // installazione aggiornamento scaricato
                            System.out.println("JBACKUP STARTER: installazione aggiornamento in corso...");
                            logger.log(Level.INFO, "JBACKUP STARTER: installazione aggiornamento in corso...");
//                        f.write("15\r\n".getBytes());

                            File fagg = new File("agg" + File.separator + "jbackup_agg.zip");
                            if (fagg.exists()) {
                                System.out.println("JBACKUP STARTER: installazione jbackup_agg.zip in corso...");
                                logger.log(Level.INFO, "JBACKUP STARTER: installazione jbackup_agg.zip in corso...");
//                            f.write("16\r\n".getBytes());
                                boolean unzipfileok = false;
                                try {
                                    int BUFFER = 2048;
                                    BufferedOutputStream dest = null;
                                    FileInputStream fis = new FileInputStream(fagg);
                                    ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                                    ZipEntry entry;
                                    while ((entry = zis.getNextEntry()) != null) {
                                        System.out.println("JBACKUP STARTER: UNZIP " + entry.getName());
//                                    f.write("17\r\n".getBytes());
                                        int count;
                                        byte data[] = new byte[BUFFER];
                                        FileOutputStream fosz = new FileOutputStream(entry.getName());
                                        dest = new BufferedOutputStream(fosz, BUFFER);
                                        while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                            dest.write(data, 0, count);
                                        }
                                        dest.flush();
                                        dest.close();
                                        fosz.close();
                                    }
                                    zis.close();
                                    unzipfileok = true;
                                } catch (Exception eunzip) {
                                    eunzip.printStackTrace();
                                }
                                //fagg.delete();
                                if (unzipfileok) {
                                    System.out.println("JBACKUP STARTER: installazione jbackup_agg.zip OK");
                                    logger.log(Level.INFO, "JBACKUP STARTER: installazione jbackup_agg.zip OK");
//                                f.write("18\r\n".getBytes());
                                    gc.aggiornaChiave("ultimo_agg_prog", "" + lf);
                                } else {
                                    System.out.println("JBACKUP STARTER: errore durante installazione jbackup_agg.zip");
                                    logger.log(Level.INFO, "JBACKUP STARTER: errore durante installazione jbackup_agg.zip");
//                                f.write("19\r\n".getBytes());
                                }
                            }

                            File flib = new File("agg" + File.separator + "jbackup_agg_lib.zip");
                            if (flib.exists()) {
                                System.out.println("JBACKUP STARTER: installazione jbackup_agg_lib.zip in corso...");
//                            f.write("20\r\n".getBytes());
                                boolean unziplibok = false;
                                try {
                                    int BUFFER = 2048;
                                    BufferedOutputStream dest = null;
                                    FileInputStream fis = new FileInputStream(flib);
                                    ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
                                    ZipEntry entry;
                                    svuotaDirLib();
                                    while ((entry = zis.getNextEntry()) != null) {
                                        System.out.println("JBACKUP STARTER: UNZIP LIB " + entry.getName());
//                                    f.write("21\r\n".getBytes());
                                        int count;
                                        byte data[] = new byte[BUFFER];
                                        FileOutputStream fosz = new FileOutputStream("lib" + File.separator + entry.getName());
                                        dest = new BufferedOutputStream(fosz, BUFFER);
                                        while ((count = zis.read(data, 0, BUFFER)) != -1) {
                                            dest.write(data, 0, count);
                                        }
                                        dest.flush();
                                        dest.close();
                                        fosz.close();
                                    }
                                    zis.close();
                                    unziplibok = true;
                                } catch (Exception eunzip) {
                                    eunzip.printStackTrace();
                                }
                                //fagg.delete();
                                if (unziplibok) {
                                    System.out.println("JBACKUP STARTER: installazione jbackup_agg_lib.zip OK");
                                    logger.log(Level.INFO, "JBACKUP STARTER: installazione jbackup_agg_lib.zip OK");
                                    
//                                f.write("22\r\n".getBytes());
                                    gc.aggiornaChiave("ultimo_agg_lib", "" + lflib);
                                } else {
//                                f.write("23\r\n".getBytes());
                                    System.out.println("JBACKUP STARTER: errore durante installazione jbackup_agg_lib.zip");
                                    logger.log(Level.INFO, "JBACKUP STARTER: errore durante installazione jbackup_agg_lib.zip");
                                }
                            }
                            svuotaDirAgg();

                            System.out.println("JBACKUP_STARTER: installazione aggiornamento TERMINATA");
                            logger.log(Level.INFO, "JBACKUP_STARTER: installazione aggiornamento TERMINATA");
//                        f.write("24\r\n".getBytes());

                            // riavvio applicazione
                            ese.uscita = true;
                            try {
                                ese.join();
                            } catch (Exception ejoin) {
                                ejoin.printStackTrace();
                            }
                            ese = new EseguiJBackup();
                            ese.start();
                        } else {
                            System.out.println("JBACKUP STARTER: errore su chiusura applicazione");
                            logger.log(Level.INFO, "JBACKUP STARTER: errore su chiusura applicazione");
//                        f.write("25\r\n".getBytes());
                        }
                    }
                } else {
                    System.out.println("JBACKUP STARTER: backup in corso");
//                f.write("26\r\n".getBytes());
                }
//            try {
//                
//                if (process != null) {
//                    if (process.exitValue() == 0) {
//                        System.out.println("Processo chiuso");
//                        System.exit(0);
//                    }
//                }
////                if (processGui != null) {
////                    if (processGui.exitValue() == 0) {
////                        System.out.println("Processo chiuso");
////                        System.exit(0);
////                    }
////                }
//            } catch (Exception e) {
//                System.out.println("Processo aperto");
//            }

                try {
                    Thread.currentThread().sleep(10000);
                } catch (Exception esleep) {
                }
//            f.close();
                num++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public static void svuotaDirAgg() {
        File fd = new File("agg");
        String[] lf = fd.list();
        for (int i = 0; i < lf.length; i++) {
            File fdel = new File("agg" + File.separator + lf[i]);
            fdel.delete();

        }
    }
    
    public static void svuotaDirLib() {
        File fd = new File("lib");
        String[] lf = fd.list();
        for (int i = 0; i < lf.length; i++) {
            File fdel = new File("lib" + File.separator + lf[i]);
            fdel.delete();

        }
    }
    

    static class EseguiJBackup extends Thread {

        public boolean uscita = false;

        public EseguiJBackup() {
        }

        public void run() {
            try {
                String s = System.getProperties().getProperty("os.name");
                if (s.toLowerCase().startsWith("win")) {
                    String par_runtime = "-Xmx1024m";
                    String[] cmd = new String[4];
//                    cmd[0] = ".." + File.separator + "jre2" + File.separator + "bin" + File.separator + "java";
                    cmd[0] = cmd[0] = System.getProperties().getProperty("java.home") + File.separator + "bin" + File.separator + "java";
                    cmd[1] = par_runtime;
                    cmd[2] = "-jar";
                    cmd[3] = "JazzBackupService.jar";
                    System.out.println("comando :" + cmd[0] + "  " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
                    ProcessBuilder pb = new ProcessBuilder(cmd);
                    pb = pb.redirectErrorStream(true);
                    process = pb.start();
                    InputStream is = process.getInputStream();
                    InputStreamReader isr = new InputStreamReader(is);
                    BufferedReader br = new BufferedReader(isr);
                    String line;
                    while ((line = br.readLine()) != null && !uscita) {
                        System.out.println(line);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    static class EseguiJBackupGui extends Thread {
//
//        public boolean uscita = false;
//
//        public EseguiJBackupGui() {
//        }
//
//        public void run() {
//            try {
//                String s = System.getProperties().getProperty("os.name");
//                if (s.toLowerCase().startsWith("win")) {
//
//                    FileOutputStream f = null;
//                    try {
//                        f = new FileOutputStream("backuploggui.txt");
//                        f.write("start\r\n".getBytes());
//
//                        String par_runtime = "-Xmx1024m";
//                        String[] cmd = new String[4];
////                    cmd[0] = ".." + File.separator + "jre" + File.separator + "bin" + File.separator + "java";
//                        cmd[0] = System.getProperties().getProperty("java.home") + File.separator + "bin" + File.separator + "java";
//                        cmd[1] = par_runtime;
//                        cmd[2] = "-jar";
//                        cmd[3] = "JazzBackup.jar";
//                        System.out.println("comando :" + cmd[0] + "  " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
//                        ProcessBuilder pb = new ProcessBuilder(cmd);
//                        pb = pb.redirectErrorStream(true);
//                        processGui = pb.start();
//                        InputStream is = processGui.getInputStream();
//                        InputStreamReader isr = new InputStreamReader(is);
//                        BufferedReader br = new BufferedReader(isr);
//                        String line;
//                        while ((line = br.readLine()) != null && !uscita) {
//                            System.out.println(line);
//                            f.write(("\r\n" + line).getBytes());
//                        }
//                        f.close();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//    private static boolean eseguiJBackup() {
//        boolean ris = false;
//        try {
//            String s = System.getProperties().getProperty("os.name");
//            if (s.toLowerCase().startsWith("win")) {
//                String par_runtime = "";
//                String[] cmd = new String[4];
//                cmd[0] = "java";
//                cmd[1] = par_runtime;
//                cmd[2] = "-jar";
//                cmd[3] = "JazzBackup.jar";
//                System.out.println("comando :" + cmd[0] + "  " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
//                ProcessBuilder pb = new ProcessBuilder(cmd);
//                pb = pb.redirectErrorStream(true);
//                process = pb.start();
//                InputStream is = process.getInputStream();
//                InputStreamReader isr = new InputStreamReader(is);
//                BufferedReader br = new BufferedReader(isr);
//                String line;
//                while ((line = br.readLine()) != null) {
//                    System.out.println(line);
//                }
//                ris = true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ris;
//    }
    public static boolean copiaFile(String fOrig, String fDest) {
        byte[] b = new byte[8192];
        FileInputStream fis = null;
        FileOutputStream fos = null;
        boolean ok = true;
        int nBlocchi, nScarto;
        boolean scarto;
        int i;

        // controllo esistenza file di origine
        File fo = new File(fOrig);
        if (!fo.exists()) {
            return false;
        }
        nBlocchi = (int) fo.length() / 8192;
        scarto = ((fo.length() % 8192) != 0);
        nScarto = (int) (fo.length() % 8192);
        // copia effettiva
        try {
            fis = new FileInputStream(fOrig);
            fos = new FileOutputStream(fDest);
            for (i = 0; i < nBlocchi; i++) {
                fis.read(b);
                fos.write(b);
            }
            // copia blocco di scarto
            if (scarto) {
                byte[] sc = new byte[nScarto];
                fis.read(sc);
                fos.write(sc);
            }
            fis.close();
            fos.close();
        } catch (Exception e) {
            ok = false;
            e.printStackTrace();
        } finally {
            try {
                fis.close();
                fos.close();
            } catch (IOException e) {
                ok = false;
            }
            return ok;
        }
    }

    public static long estraiLong(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+'
                    || s.charAt(i) == '-'
                    || Character.getType(s.charAt(i))
                    == Character.DECIMAL_DIGIT_NUMBER) {
                ris = ris + s.charAt(i);
            }
        }
        try {
            return (new Long(ris)).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int estraiIntero(String s) {
        String ris = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+'
                    || s.charAt(i) == '-'
                    || Character.getType(s.charAt(i))
                    == Character.DECIMAL_DIGIT_NUMBER) {
                ris = ris + s.charAt(i);
            }
        }
        if (ris.length() > 1) {
            if (ris.substring(ris.length() - 1).equals("-")) {
                ris = ris.substring(ris.length() - 1) + ris.substring(0, ris.length() - 1);
            } else if (ris.substring(ris.length() - 1).equals("+")) {
                ris = ris.substring(0, ris.length() - 1);
            }
        }
        try {
            return (new Integer(ris)).intValue();
        } catch (Exception e) {
            return 0;

        }
    }

    static class JBackupShutdown extends Thread {

        public void run() {
            try {
                process.destroy();
//                processGui.destroy();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
